// const ipbxAdmin = require("./endpoints");
import ipbxAdmin from "./endpoints";

class Session {
    async login(login, password) {

        let ipbxAdminEndpoint = await ipbxAdmin.getInstance();
        return ipbxAdminEndpoint.session.login({
            user: login,
            passwd: password,
            mode: "user"
        })
    }
}

// module.exports = new Session();

export default new Session();
