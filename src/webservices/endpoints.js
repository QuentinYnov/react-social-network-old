import cookie from 'react-cookies';

// =========================================================
// ========== Débogueur ====================================
// =========================================================

class ipbxDebug {
    constructor() {
        this.debug = false;
    }

    warn(t, m) {
        if (this.debug) console.warn(t, m);
    }

    log(t, m) {
        if (this.debug) console.log(t, m);
    }

    dir(t, m) {
        if (this.debug) {
            console.log(t);
            console.dir(m);
        }
    }

    alert(m) {
        if (this.debug) alert(m);
    }
}

// =========================================================
// ============= endpoint(s) ===============================
// =========================================================


class ipbxEndpoint {
    constructor(url) {
        this.services = {};
        this.isReady = false;
        this.url = url;
    }

    getServices() {
        if (!this.isReady) alert("'" + this.url + "' nest pas prêt");
        return this.services;
    }


    async ready() {
        if (this.isReady) return;

        const rep = await fetch(this.url + '?wsdl&format=json');
        const services = await rep.json();

        for (const service of services) {
            this.services[service] = null;
        }

        this.isReady = true;
    }

}

class ipbxServiceProxy {
    constructor() {
    }

    get(target, method) {
        return target.soapCall.bind(target, method)
    }
}

class ipbxXML {
    /* TODO
     gérer les <element nillable="true" maxOccurs="unbounded">
     nillable apparait uniquement dans les paramètres d'entrée (peut-il être en sortie ??)
     le seul maxOccurs est "unbounded"
     */

    constructor(schema) {
        this.types = {};

        for (const type of schema.childNodes) {
            const typeName = type.getAttribute('name');
            if (type.nodeName == 'xsd:complexType') {
                this.types['tns:' + typeName] = type;
                this.types['tns:' + typeName + '[]'] = type;
            } else if (type.nodeName == 'xsd:element') {
                this.types[typeName] = type;
            } else {
                // normalement la balise import
            }
        }
    }

    // Dans toutes les méthodes suivante node est un NodeList (ou parfois une liste de Node)
    // Même si la plupart du temps il ne contient qu'un seul élément
    faultToJson(node) {
        node = this.filter(node);
        let obj = {};
        for (let item of node[0].childNodes) {
            obj[item.nodeName] = item.textContent;
        }
        return obj;
    }

    toJson(typeName, node) {
        const type = this.types[typeName];
//    console.dir(['json', type, node]);
        // ici il peut y avoir 3 sortes de type
        let nodeValue;
        if (type.childElementCount == 0) {
            nodeValue = this.getElement(type, node);
        } else if (type.nodeName == 'xsd:complexType') {
            nodeValue = this.getComplexType(type, node);
        } else {
            nodeValue = this.getComplexType(type.childNodes[0], node);
        }

        return nodeValue;
    }

    getElement(type, node) {
//    console.dir(['element', type, node]);
        // type est un <node> sans fils

        let nodeValue;
        const nodeType = type.getAttribute('type');
        node = this.filter(node);

        if (node.length == 0) {
            console.log('NULL ' + nodeType + ' ' + type.getAttribute('name'));
            nodeValue = undefined;
        } else if (nodeType.startsWith('xsd:')) {
            const nodeText = node[0].textContent;
            if (nodeText == '') {
                nodeValue = null;
            } else if (nodeType == 'xsd:boolean') {
                nodeValue = Boolean(Number(nodeText));
            } else if (nodeType == 'xsd:int') {
                nodeValue = parseInt(Number(nodeText));
            } else if (nodeType == 'xsd:string') {
                nodeValue = nodeText;
            } else if (nodeType == 'xsd:time') {
                nodeValue = Date(Number(nodeText));
            } else {
                console.log('XXX type inconnu : ' + nodeType);
                nodeValue = null;
            }
        } else {
            nodeValue = this.toJson(nodeType, node);
        }

        return nodeValue;
    }

    getComplexType(type, node) {
//    console.dir(['complexe', type, node]);
        /*
         contient
         <xsd:complexContent> => tableau
         <xsd:sequence>       => objet
         <xsd:all>            => objet
         ou rien
         */

        let nodeValue;
        type = type.childNodes[0];
        if (type == undefined) {
            console.log('Empty complexType');
            nodeValue = null;
        } else if (type.nodeName == 'xsd:complexContent') {
            nodeValue = this.getArray(type, node);
        } else if (type.nodeName == 'xsd:all' || type.nodeName == 'xsd:sequence') {
            nodeValue = this.getObj(type, node);
        } else {
            console.log('XXX noeud inconnu : ' + type.nodeName);
            nodeValue = null;
        }
        return nodeValue
    }

    getArray(type, nodes) {
        /*
         Dans le xml,
         un tableau avec des éléments est :
         <arraytag>
         <element /> <!-- sans le tag de l'élément !>
         </arraytag>
         <arraytag>
         <element /> <!-- sans le tag de l'élément !>
         <arraytag>
         un tableau avec un élément est :
         <arraytag>
         <element /> <!-- sans le tag de l'élément !>
         </arraytag>
         un tableau vide est :
         <arraytag>
         </arraytag>
         */

        let array = [];
        // teste si le tableau n'est pas vide :
        if (nodes[0].childElementCount > 0) {
            const typeName = type.childNodes[0].childNodes[0].getAttribute('wsdl:arrayType');
            for (const node of nodes) {
                array.push(this.toJson(typeName, [node]));
            }
        }
        return array;

    }

    getObj(type, node) {

        node = this.filter(node);
        /*console.log(['obj', type, node]);*/
        let obj = {};
        for (var i = 0; i < type.childElementCount; i++) {
            const propType = type.childNodes[i];
            const propName = propType.getAttribute('name');

            if (propType.nodeName == 'xsd:element') {
                // const typeName = propType.getAttribute('type');
                // obj[propName] = this.toJson(typeName, node[0].querySelectorAll('*>'+propName.replace('_','-')));
                if (!node[0]) {
                    console.dir([obj, propName, node]);
                }
                obj[propName] = this.getElement(propType, node[0].querySelectorAll('*>' + propName.replace('_', '-')));
            } else {
                console.log('XXX noeud inconnu : ' + propType.nodeName);
                obj[propName] = 'XXX noeud inconnu';
            }
        }
        return obj;
    }

    /**
     supprime les noeuds textuels du niveau root contenant que des whitespace
     @param node nodeList ou node[]
     @return node[]
     */
    filter(node) {
        let filter = [];
        for (var n = 0; n < node.length; n++) {
            var child = node[n];
            if (
                child.nodeType === Node.COMMENT_NODE
                ||
                (child.nodeType === Node.TEXT_NODE && !/\S/.test(child.nodeValue))
            ) {
            } else if (child.nodeType === Node.ELEMENT_NODE) {
                filter.push(child);
            }
        }
        return filter;
    }

}

// =========================================================
// ============= webservice ================================
// =========================================================
// =========================================================
// ========== WSDL interface ===============================
// =========================================================
class ipbxWSDL {

    constructor(myParent) {
        this.xmlConverter = null;
        this.jsonConverter = null;
        this.service = myParent;
        this.methods = [];
        this.isReady = false;
    }

    async getMethods() {
        await this.ready();
        return this.methods;
    }

    fromSoap(tag, xml) {
        const responseXML = (new window.DOMParser()).parseFromString(xml, "text/xml");
        const fault = responseXML.querySelectorAll('Envelope>Body>Fault');
        if (fault.length) {
            throw this.xmlConverter.faultToJson(fault);
        } else {
            const nodeName = tag + 'Response';
            const node = responseXML.querySelectorAll('Envelope>Body>' + nodeName);
            return this.xmlConverter.toJson(nodeName, node).response; // oui pour ipbx chaque réponse possède ->reponse
        }
    }

    toSoap(tag, obj) {
        return '<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope"><soap:Body>'
            + this.jsonConverter.toXML(tag, obj)
            + '</soap:Body></soap:Envelope>';
    }

    async ready() {
        if (this.isReady) return;

        const rep = await fetch(this.service.endpoint.url + '?wsdl=' + this.service.name);
        const wsdl = await rep.text();

        const wsdlXML = (new window.DOMParser()).parseFromString(wsdl, "text/xml");
        this.clean(wsdlXML);

        const schema = wsdlXML.querySelectorAll('definitions>types>schema')[0];
        this.xmlConverter = new ipbxXML(schema);

        const ns = wsdlXML.childNodes[0].getAttribute('targetNamespace').replace('urn:', '');
        this.jsonConverter = new ipbxJSON(ns);

        let methods = wsdlXML.querySelectorAll('definitions>portType>operation');
        for (var i = 0; i < methods.length; i++) { // pas de for( in )
            this.methods.push(methods[i].getAttribute('name'));
        }

        this.isReady = true;
    }

    /**
     supprime récursivement sur place les noeuds textuels contenant que des whitespace
     @param node nodeList
     */
    clean(node) {
        for (var n = 0; n < node.childNodes.length; n++) {
            var child = node.childNodes[n];
            if (
                child.nodeType === Node.COMMENT_NODE
                ||
                (child.nodeType === Node.TEXT_NODE && !/\S/.test(child.nodeValue))
            ) {
                node.removeChild(child);
                n--;
            } else if (child.nodeType === Node.ELEMENT_NODE) {
                this.clean(child);
            }
        }
    }

}

class ipbxService extends ipbxDebug {
    constructor(endpoint, serviceName) {
        super();
//    this.debug = true;

        this.name = serviceName;
        this.endpoint = endpoint;
        this.wsdl = new ipbxWSDL(this);
    }

    async soapCall(method, params) {
        return this.wsdl.getMethods()
            .then((methods) => {
                if (methods.indexOf(method) > -1) {
                    return this._soapCall(method, params)
                } else {
                    this.warn("methode inconnue :", method);
                    this.dir('methodes :', methods);
                    throw {
                        service: this.name,
                        error: 'méthode inconnue : ' + method,
                    }
                }
            });
    }

    _soapCall(method, params) {
        this.log('XML REQUEST', this.wsdl.toSoap(method, params));

        let headers = new Headers();
        headers.append("Authorization", "Basic " + btoa(cookie.load("IPBX_SESSION") + ":dummy"));
        console.log(headers);
        return fetch(
            this.endpoint.url,
            {
                credentials: 'same-origin',
                headers: headers,
                method: 'post',
                body: this.wsdl.toSoap(method, params)
            }
        )
            .then((rep) => {
                return rep.text()
            })
            .then((xml) => {
                this.log('XML response', xml);
                return this.wsdl.fromSoap(method, xml);
            })
            .catch((soapFault) => {
                this.dir(soapFault.faultcode, [method, params]);
                this.alert(soapFault.faultstring);
                throw soapFault;
            })
    }
}

class ipbxEndpointProxy extends ipbxDebug {
    constructor() {
        super();
        this.debug = false;
    }

    get(target, service) {
        // les méthods async lancent des .then sur le proxy (???)
        if (service == 'then') return target.then;

        const services = target.getServices();
        if (service in services) {
            if (!services[service]) {
                services[service] = new Proxy(new ipbxService(target, service), new ipbxServiceProxy());
            }
            return services[service];
        } else {
            this.warn("service inconnu :", service);
            this.dir('services :', Object.keys(services));
            return null;
        }
    }

}


// =========================================================
// ========== Convertit un objet json en XML ===============
// =========================================================


class ipbxJSON {
    constructor(ns) {
        this.ns = ns;
    }

    toXML(tag, obj, noNamespace) {
        let attribute = '';
        if (!noNamespace) {
            attribute = ' xmlns:' + this.ns + '="urn:' + this.ns + '"';
        }
        const openTag = '<' + this.ns + ':' + tag + attribute + '>';
        const closeTag = '</' + this.ns + ':' + tag + '>';
        let xml = '';
        if (obj == null) {
        } else if (obj instanceof Array) {
            for (var i in obj) {
                xml += this.toXML(tag, obj[i]);
            }
        } else if (obj instanceof Date) {
            xml = String(obj.getTime() / 1000);
        } else if (obj instanceof Boolean) {
            xml = obj ? '1' : '0';
        } else if (obj instanceof Object) {
            for (var prop in obj) {
                xml += this.toXML(prop, obj[prop], true);
            }
        } else {
            xml = String(obj);
        }
        return openTag + xml + closeTag
    }
}

// =========================================================
// ========== Convertit un objet XML en Json ===============
// =========================================================


let ipbxEndpoints = {
    endpoints: {},
    get: async function (url) {
        if (!(url in this.endpoints)) {
            const endpoint = new ipbxEndpoint(url);
            await endpoint.ready();
            this.endpoints[url] = new Proxy(endpoint, new ipbxEndpointProxy());
        }
        return this.endpoints[url];
    }
}

// ============ définition des endpoints ==================
async function ipbxConnect() {
    // point d'entrée principal
    // TODO server IP in localstorage
    return await ipbxEndpoints.get('http://192.168.0.200/fcgi/ipbx_admin.fcgi');
    // module RECORDING
    //ipbxRecording  = await ipbxEndpoints.get('/recording');
    // module FAX
    //ipbxFax        = await ipbxEndpoints.get('/fax');
    // module IVS
    // ipbxSvi        = await ipbxEndpoints.get('/svi');
    // module CTC
    //ipbxCti        = await ipbxEndpoints.get('/ctc');
    // module STATISTICS
    //ipbxStat       = await ipbxEndpoints.get('/statistics');
    // module ACD
    //ipbxAcd        = await ipbxEndpoints.get('/acd');
    // module CONFERENCE
    //ipbxConference = await ipbxEndpoints.get('/conference');
}

class ipbxAdmin {
    ipbxAdminInstance = null;

    async getInstance() {
        if (!this.ipbxAdminInstance) {
            this.ipbxAdminInstance = await ipbxConnect();
        }

        return this.ipbxAdminInstance;
    }
}

// module.exports = new ipbxAdmin();
export default new ipbxAdmin();