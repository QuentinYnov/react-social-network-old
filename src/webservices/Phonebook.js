// const ipbxAdmin = require("./endpoints");
import ipbxAdmin from  "./endpoints";



class Phonebook {

  async list() {
    let ipbxAdminEndpoint = await ipbxAdmin.getInstance();
    return ipbxAdminEndpoint.phonebook.list({
      from: "ListMyPhonebook"
    })
  }

  async get(phoneBookId) {
    let ipbxAdminEndpoint = await ipbxAdmin.getInstance();
    return ipbxAdminEndpoint.phonebook.get({
      from: "ListMyPhonebook",
      phoneId: phoneBookId
    })
  }
}

// module.exports = new Phonebook();
export default new Phonebook()