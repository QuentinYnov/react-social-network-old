import React from 'react';
import ReactDom from 'react-dom';
import App from './App';
import { Provider } from 'react-redux';
import sampleData from './initialeState';
import configureStore, { history } from "./store"

const store = configureStore();

ReactDom.render(
    <Provider store={store} >
        <App history={history}/>
    </Provider>, document.getElementById("root"));