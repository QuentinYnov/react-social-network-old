import React from 'react';
import { Route } from "react-router-dom";
import { ConnectedRouter } from 'connected-react-router';
import SignIn from './components/containers/sign-in';
import SignUp from './components/containers/sign-up';
import Home from './components/containers/Home';
import listContact from './components/containers/listContact';
import message from './components/containers/message';
import CallHistory from './components/containers/CallHistory';
import settings from './components/containers/settings';
import profile from './components/containers/profile';
import showProfile from './components/containers/showProfile';



const App = ({ history }) => {
    return (

        <ConnectedRouter history={history}>
            <div>
                <Route exact path="/connexion" component={SignIn}/>
                <Route exact path="/inscription" component={SignUp}/>
                <Route exact path="/" component={Home}/>
                <Route exact path="/contact" component={listContact}/>
                <Route exact path="/historique" component={CallHistory}/>
                <Route exact path="/message" component={message}/>
                <Route exact path="/settings" component={settings}/>
                <Route exact path="/profile" component={profile}/>
                <Route exact path="/showprofile" component={showProfile}/>
            </div>
        </ConnectedRouter>
    )
};

export default App;
