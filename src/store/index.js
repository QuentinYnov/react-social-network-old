import appReducer from './reducers';
import { applyMiddleware, compose, createStore } from 'redux'
import thunk from 'redux-thunk'
import checkAuth from "../middleware/checkAuth";
import consoleMessages from "../middleware/consoleMessages";
import { routerMiddleware } from 'connected-react-router'

import { createBrowserHistory } from 'history'

// export const store = storeFactory(sampleData);

export const history = createBrowserHistory();

export default function configureStore(preloadedState) {
    return createStore(
        appReducer(history), // root reducer with router state
        preloadedState,
        compose(
            applyMiddleware(
                routerMiddleware(history),
                consoleMessages,
                checkAuth(history),
                thunk
            ),
        ),
    )
}