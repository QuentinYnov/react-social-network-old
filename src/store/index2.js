import appReducer from './reducers';
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import C from '../constants';

const consoleMessages = store => next => action => {

    let result;

    console.log(`dispatching action => ${action.type}`);
    result = next(action);
    console.log('store')
    console.log(store.getState());

    return result

};

export default (initialState={}) => {
    return applyMiddleware(thunk, consoleMessages)(createStore)(appReducer, initialState)
}
