import C from '../constants';
import {combineReducers} from 'redux'
import { connectRouter } from 'connected-react-router'

export const number = (number = "", action) => {

    switch (action.type) {
        case C.PRESS_KEY:
            return number + action.payload;
        case C.REMOVE_NUMBER:
            return number.slice(0,-1);
        case C.GET_CONTACT:
            return action.payload;
        default:
            return number
    }
};

export const msgValue = (msgValue = "", action) => {

    if (action.type === C.SEND_MESSAGE) {
        return action.payload
    }
    return msgValue
};

export const msgInput = (msgInput = "", action) => {
    switch (action.type) {
        case C.WRITE_MESSAGE:
            return action.payload;
        case C.RESET_INPUT:
            return msgInput = "";
        default:
            return msgInput
    }
};

export const fetching = (fetching=false, action) => {

    switch(action.type) {

        case C.FETCHING_ALL_CONTACT :
            return true;

        case C.FAIL_CONTACT :
            return false;

        case C.DONE_CONTACT :
            return false;

        default:
            return fetching
    }

};

export const contacts = (contacts=[], action) => {

    switch(action.type) {

        case C.DONE_CONTACT :
            return action.payload;

        default:
            return contacts;
    }

};

export const fetchingOne = (fetchingOne = false, action) => {
    switch (action.type) {

        case C.FETCHING_ALL_CONTACT :
            return true;
        case C.FAIL_ONE_CONTACT :
            return false;
        case C.GET_CONTACT :
            return false;
        default:
            return fetchingOne
    }
};

export const oneContact = (oneContact = [], action) => {

    switch (action.type){
        case C.GET_CONTACT :
            return action.payload;
        default :
            return oneContact
    }
};

export const fetchLogin = (fetchLogin = false, action) =>{
    switch (action.type){
        case C.FETCH_LOGIN :
            return true;
        case C.SUCCESS_LOGIN:
            return false;
        case C.FAIL_LOGIN:
            return false;
        default:
            return fetchLogin
    }
};

export const statusLogin = (statusLogin = C.DISCONNECT, action = {}) =>{
    switch (action.type){
        case C.SUCCESS_LOGIN:
            return C.SUCCESS_LOGIN;
        case C.DISCONNECT:
            return C.DISCONNECT;
        case C.FAIL_LOGIN:
            return C.FAIL_LOGIN;
        default:
            return statusLogin
    }
};

export const push = (push = '/', action) =>{
    switch (action.type){
        case C.PUSH :
            return action.payload;
        default:
            return push;
    }
}

export default (history) => combineReducers({
    number,
    msgValue,
    msgInput,
    contact: combineReducers({
        fetching,
        contacts
    }),
    contactGet: combineReducers({
        fetchingOne,
        oneContact,
    }),
     fetchLogin,
     statusLogin,
    router: connectRouter(history),
    push,
})