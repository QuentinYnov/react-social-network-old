const consoleMessages = store => next => action => {

    let result;

    console.log(`dispatching action => ${action.type}`);
    result = next(action);
    console.log('store')
    console.log(store.getState());

    return result

};

export default consoleMessages;