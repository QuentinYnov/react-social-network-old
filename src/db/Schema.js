const profileSchema = {
    'title': 'profile schema',
    'description': 'profile\'s description',
    'version': 0,
    'type': 'object',
    'properties': {
        'id': {
            'type': 'string',
            'primary': true
        },
        'login': {
            'type': 'string',
        },
        'password': {
            'type': 'string',
        },
        'local_ip': {
            'type': 'string',
        },
        'remote_vpn_ip': {
            'type': 'string',
        },
        'remote_vpn_port': {
            'type': 'string',
        },
        'ca_server': {
            'type': 'string',
        },
        'ca_client': {
            'type': 'string',
        },
        'client_key': {
            'type': 'string',
        },
        'server_vpn_ip': {
            'type': 'string',
        },
        'parameters': {
            'type': 'string'
        }
    }
};


export default profileSchema;
