import C from './constants';
import Phonebook from './webservices/Phonebook';
import cookie from "react-cookies";
import Session from "./webservices/Session";
import { push } from 'connected-react-router';

export function addNumber(numberAdd) {
    return {
        type: C.PRESS_KEY,
        payload: numberAdd,
    };
}

export function resetNumber() {
    return {type: C.REMOVE_NUMBER}
}

export function sendMessage(msgInput) {
    return {
        type: C.SEND_MESSAGE,
        payload: msgInput,
    }
}

export function writeMessage(textWritten) {
    return {
        type: C.WRITE_MESSAGE,
        payload: textWritten
    }
}

export function resetInput() {
    return {
        type: C.RESET_INPUT
    }
}

export const fetchContact = () => dispatch => {
    dispatch({
        type: C.FETCHING_ALL_CONTACT
    });

    Phonebook.list().then((response) => {
        dispatch({
            type: C.DONE_CONTACT,
            payload: response.Contacts
        });
        console.log(response.Contacts);
    }).catch((err) => console.log(err));
};

export const getContact = (contactId) => dispatch => {

    dispatch({

        type: C.FETCHING_GET_CONTACT,
    });
    console.log(contactId)
    Phonebook.get(contactId).then((contact) => {
        dispatch({
            type: C.GET_CONTACT,
            payload: contact
        });
        console.log(contact);
    })
};

export const onLogin = (login, password) => dispatch => {

    console.log(login,' | ',password);
    dispatch({
        type: C.FETCH_LOGIN,
    });

    Session.login(login, password)
        .then((response) => {
        dispatch({
            type: C.SUCCESS_LOGIN,
            payload: response.session
        });
        cookie.save('IPBX_MODE', "user");
        cookie.save('IPBX_SESSION', response.session);
        console.log(response.valueOf());
        console.log(cookie.loadAll());
    })
    .catch((err) => {
        dispatch({
           type: C.FAIL_LOGIN
        });
        console.log(err)
        return false;
    });
};

export const getNumber = (number) => {
    return {
        type: C.GET_CONTACT,
        payload: number,
    }
};

export const disconnect = () => dispatch => {

    cookie.remove('IPBX_MODE');
    cookie.remove('IPBX_SESSION');
    console.log(cookie.loadAll());

    return {
        type: C.DISCONNECT
    };
};

export const test = (msg) => dispatch => {
    dispatch({
        type: C.TEST,
        payload: msg,
    });
    console.log('test');

    return {
        type: C.TEST,
        payload: msg,
    }
}

export const pushRoute = (path, statusLogin) => dispatch => {
    if(statusLogin === C.DISCONNECT){
        dispatch(push("/connexion"));
    }
    else{
        dispatch(push(path));
    }
}
