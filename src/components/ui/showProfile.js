import React, {Component} from 'react';
import * as Database from '../../db/Database';

class ProfileInsert extends Component {
    state = {
        name: '',
        login: '',
        password: '',
        local_ip: '',
        remote_vpn_ip: '',
        remote_vpn_port: '',
        ca_server: '',
        ca_client: '',
        client_key: '',
        server_vpn_ip: '',
        parameters: '',
    }
    subs = []

    addProfile = async (event) => {
        event.preventDefault()
        const {name, color} = this.state;
        const db = await Database.get();
        db.heroes.insert({name, color});
        this.setState({name: '', color: ''});
    }
    handleNameChange = (event) => {
        this.setState({name: event.target.value});
    }
    handleLoginChange = (event) => {
        this.setState({login: event.target.value});
    }
    handlePasswordChange = (event) => {
        this.setState({password: event.target.value});
    }
    handleLocalIpChange = (event) => {
        this.setState({local_ip: event.target.value});
    }
    handleRemoteVpnIpChange = (event) => {
        this.setState({remote_vpn_ip: event.target.value});
    }
    handleRemoteVpnPortChange = (event) => {
        this.setState({remote_vpn_port: event.target.value});
    }
    handleLoginChange = (event) => {
        this.setState({login: event.target.value});
    }
    handleCaServerChange = (event) => {
        this.setState({ca_server: event.target.value});
    }
    handleCaClientChange = (event) => {
        this.setState({ca_client: event.target.value});
    }
    handleClientKeyChange = (event) => {
        this.setState({client_key: event.target.value});
    }
    handleServerVpnIpChange = (event) => {
        this.setState({server_vpn_ip: event.target.value});
    }
    handleParametersChange = (event) => {
        this.setState({parameters: event.target.value});
    }

    render() {
        return (
            <div id="insert-box" className="box">
                <h3>Add Profile</h3>
                <form onSubmit={this.addProfile}>
                    <input type="text" placeholder="Name" value={this.state.name} onChange={this.handleNameChange} />
                    <input type="text" placeholder="Login" value={this.state.login} onChange={this.handleLoginChange} />
                    <input type="text" placeholder="Password" value={this.state.password} onChange={this.handlePasswordChange} />
                    <input type="text" placeholder="Local IP" value={this.state.local_ip} onChange={this.handleLocalIpChange} />
                    <input type="text" placeholder="Remote VPN IP" value={this.state.remote_vpn_ip} onChange={this.handleRemoteVpnIpChange} />
                    <input type="text" placeholder="Remote VPN port" value={this.state.remote_vpn_port} onChange={this.handleRemoteVpnPortChange} />
                    <input type="text" placeholder="CA Server" value={this.state.ca_server} onChange={this.handleCaServerChange} />
                    <input type="text" placeholder="CA Client" value={this.state.ca_client} onChange={this.handleCaClientChange} />
                    <input type="text" placeholder="Client key" value={this.state.client_key} onChange={this.handleClientKeyChange} />
                    <input type="text" placeholder="Server VPN IP" value={this.state.server_vpn_ip} onChange={this.handleServerVpnIpChange} />

                    <input type="text" placeholder="Parameters" value={this.state.parameters} onChange={this.handleParametersChange}/>
                    <button type="submit">Créer le profil</button>
                </form>
            </div>
        );
    }
}

export default ProfileInsert;
