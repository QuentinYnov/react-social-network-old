import {PropTypes} from 'prop-types'
import React from 'react';
import '../../App.css';
import Header from '../containers/header'
import Toolbar from '../containers/footer'


const Message = ({msgValue, msgInput, onSendMessage, onWriteMessage}) => {



    return (
        <div id="undersms">
            <Header/>
            <div className="message">
                <div className="sender">
                    <div id="textsmssender">{msgValue}</div>
                </div>
                <div className="receiver">
                    <div id="textsmsreceiver">Contact : Ceci est le message reçu</div>
                </div>
            </div>
            <div className="champbtn">
                <textarea className="smsinput" onChange={(event) => onWriteMessage(event.target.value)} value = {msgInput}/>
                <i className="fas fa-paper-plane fa-2x cross2" onClick={() => onSendMessage(msgInput)}></i>


            </div>
            <Toolbar/>
        </div>
    );


};

export default Message;