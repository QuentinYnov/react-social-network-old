import {PropTypes} from 'prop-types'
import React from 'react';
import '../../App.css';
import Header from '../containers/header';
import Footer from '../containers/footer';
import Key from '../containers/key';
import 'react-s-alert/dist/s-alert-default.css';
import 'react-s-alert/dist/s-alert-css-effects/slide.css';
import {StyleSheet, Text} from 'react-native';


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});


const Home = ({ number, onResetNumber, onAddNumber}) => {

    function renderKey (keyValue) {
        return (<Key
            keyValue={keyValue}
            onClick={() => onAddNumber(number)}
        />);
    }

    /*function testDb(){
        //model.initDb(app.getPath("userData"));
        model.getProfile();
        model.getConnexion();
    }*/

    function callNumber () {
        console.log('Appeler le'.number);
        /*const args = {
            number: number, // String value with the number to call
            prompt: false // Optional boolean property. Determines if the user should be prompt prior to the call
        }

        call(args).catch(console.error);
        */
        return console.log(number);
    }

    return (

        <div className="App">
            <Header/>

            <Footer/>
        </div>

    );


};
Home.PropsType = {}

export default Home;
