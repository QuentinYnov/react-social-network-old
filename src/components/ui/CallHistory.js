import {PropTypes} from 'prop-types'
import React from 'react';
import '../../App.css';
import Header from '../containers/header';
import Toolbar from '../containers/footer';


const CallHistory = () =>{

    return(
        <div id="history">
            <Header/>
            <div className="historic_container">
                <div className="oneHistoric">
                    <div className="callerProfile">
                        <div className="callerImg"><i className="far fa-user-circle fa-3x"></i></div>
                        <div className="callerName">Laurent</div>
                    </div>
                    <div className="callDetail">
                        <div className="constant">
                            <div>Date :</div>
                            <div>Durée :</div>
                        </div>
                        <div className="entries">
                            <div className="callDate">20/06/2018</div>
                            <div className="callTime">00.20.34</div>
                        </div>
                    </div>
                </div>

                <div className="oneHistoric">
                    <div className="callerProfile">
                        <div className="callerImg"><i className="far fa-user-circle fa-3x"></i></div>
                        <div className="callerName">Michel</div>
                    </div>
                    <div className="callDetail">
                        <div className="constant">
                            <div>Date :</div>
                            <div>Durée :</div>
                        </div>
                        <div className="entries">
                            <div className="callDate">20/06/2018</div>
                            <div className="callTime">00.20.34</div>
                        </div>
                    </div>
                </div>

                <div className="oneHistoric">
                    <div className="callerProfile">
                        <div className="callerImg"><i className="far fa-user-circle fa-3x"></i></div>
                        <div className="callerName">Antoine</div>
                    </div>
                    <div className="callDetail">
                        <div className="constant">
                            <div>Date :</div>
                            <div>Durée :</div>
                        </div>
                        <div className="entries">
                            <div className="callDate">20/06/2018</div>
                            <div className="callTime">00.20.34</div>
                        </div>
                    </div>
                </div>
            </div>
            <Toolbar/>
        </div>
    )
};

export default History;