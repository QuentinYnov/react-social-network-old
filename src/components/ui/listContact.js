import {PropTypes} from 'prop-types'
import React from 'react';
import '../../App.css';
import {Link} from 'react-router-dom'
import Header from '../containers/header'
import Toolbar from '../containers/footer'
import C from '../../constants';


class ListContact extends React.Component {

    displayContact() {
        if (!this.props.fetching) {
            this.props.onLoadContact();
        }
    }

    onFetching() {
        if (this.props.fetching) {
            return (
                <img src={require('../../img/loader.svg')} className="load"/>
            )
        }
    }

    CheckLogin() {
        if (this.props.statusLogin === C.DISCONNECT) {
            this.props.history.push('/connexion')
        }
    }

    componentDidMount(){
        this.CheckLogin()
    }

    render() {
        return (
            <div id="underitneo">
                <Header/>
                <div className="contactList">
                    {this.onFetching()}
                    page de contact
                    <button onClick={() =>  this.displayContact()} className="contactLoader">CONTACT</button>

                    {this.props.contacts.map((contact) => {
                        let key;
                        let id;
                        //TODO WEBSERVICE USER TO GET USER WHEN CLICKED, CURRENTLY USER NOT DISPLAYED
                        if (contact.PhoneBookId === 0) {
                            key = "user_" + contact.PhoneBookUserId;
                            id = contact.PhoneBookUserId;
                        }
                        else {
                            key = "contact_" + contact.PhoneBookId;
                            id = contact.PhoneBookId;

                            return (
                                <div className="cadrecontact" key={key}>
                                    <Link to={"/contactdetail/" + id}>
                                        <div className="contactContainer">
                                            <div>{<img src={require("../../img/contact.png")} height={94} width={94}
                                                       alt="contact"/>}</div>
                                            <div className="nameContact">{contact.UserFirstnameLastname} n° de
                                                poste: {contact.PhoneBookStandardPhone}</div>
                                        </div>
                                    </Link>
                                </div>
                            )
                        }

                    })}
                </div>
                <Toolbar/>
            </div>
        )

    };

};


export default ListContact;