import {PropTypes} from 'prop-types'
import React from 'react';
import '../../App.css';
import Header from '../containers/header';
import Toolbar from '../containers/footer';
import {Link} from 'react-router-dom'

class ContactDetail extends React.Component {

    componentDidMount() {
        this.props.onLoadOneContact(this.props.match.params.contactId)
    }

    render() {
        return (
            <div>
                <div id="underlist">
                    <Header/>
                    {console.log(this.props.oneContact)}
                    <div className="cadreprofil">
                        <div className="contactDetailContainer">
                            <div className="topDetailContainer">
                                <i className="far fa-user-circle fa-3x"/>
                                <div
                                    className="contactName">{this.props.oneContact.PhoneBookLastname + " " + this.props.oneContact.PhoneBookFirstname} </div>
                            </div>
                            <div className="otherDetail">
                                <div className="contactCompagny">Compagnie
                                    : {this.props.oneContact.PhoneBookCompagny}</div>
                                <div className="contactCategory">Categorie du contact
                                    : {this.props.oneContact.PhoneBookCategory}</div>
                                <div className="contactPhoneNumber">Numéro standard
                                    : {this.props.oneContact.PhoneBookStandardPhone}
                                    <Link to="/">
                                        <i className="fas fa-phone-square littlephone"
                                           onClick={() => this.props.sendnumber(this.props.oneContact.PhoneBookStandardPhone)}/>
                                    </Link>
                                </div>
                                <div className="contactDirectPhone">Numéro direct
                                    : {this.props.oneContact.PhoneBookDirectPhone}
                                    <Link to="/">
                                        <i className="fas fa-phone-square littlephone"
                                           onClick={() => this.props.sendnumber(this.props.oneContact.PhoneBookDirectPhone)}/>
                                    </Link>
                                </div>
                                <div className="contactPersonPhone">Numéro personel
                                    : {this.props.oneContact.PhoneBookPersoPhone}
                                    <Link to="/">
                                        <i className="fas fa-phone-square littlephone"
                                           onClick={() => this.props.sendnumber(this.props.oneContact.PhoneBookPersoPhone)}/>
                                    </Link>
                                </div>
                                <div className="contactEmail">Email : {this.props.oneContact.PhoneBookEmai}</div>
                                <div className="contactFax">Fax : {this.props.oneContact.PhoneBookFax}</div>
                            </div>
                        </div>
                    </div>
                    <Toolbar/>
                </div>
            </div>
        )
    }
};


export default ContactDetail;
