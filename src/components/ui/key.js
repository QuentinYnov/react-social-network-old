import React from 'react';
import '../../App.css'

const Key = ({onAddNumber, keyValue}) => {

    return (
        <button className="key" onClick={() => onAddNumber(keyValue)}>
            {keyValue}
        </button>
    );


};

export default Key;