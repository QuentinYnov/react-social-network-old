import React, {Component} from 'react';
import * as Database from '../../db/Database';
import Header from '../containers/header';
import {TextInput, View, Button, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#eeeeee',
        alignItems: 'center',
        justifyContent: 'center',
    },
});


class ProfileInsert extends Component {
    state = {
        name: '',
        login: '',
        password: '',
        local_ip: '',
        remote_vpn_ip: '',
        remote_vpn_port: '',
        ca_server: '',
        ca_client: '',
        client_key: '',
        server_vpn_ip: '',
        parameters: '',
    }

    addProfile = async (event) => {
        event.preventDefault()
        const {name, value} = this.state;
        const db = await Database.get();
        db.profiles.insert({name, value});
        this.setState({name: '', value: ''});
    }

    handleKeyPress(target) {
        if(target.key=='Enter'){
            console.log(target.key);
            //this.props.onSignIn(this.state.login, this.state.password);
        }
    }

    handleChangeInput(stateName, value) {
        let newState = this.state;
        newState[stateName] = newState[stateName]+value;
        this.setState(newState);
        console.log(this.state);
    }

    //updates the text inputs
    /*handleNameChange = (event) => {
        this.setState({name: event.target.value});
    }
    handleLoginChange = (event) => {
        this.setState({login: event.target.value});
    }
    handlePasswordChange = (event) => {
        this.setState({password: event.target.value});
    }
    handleLocalIpChange = (event) => {
        this.setState({local_ip: event.target.value});
    }
    handleRemoteVpnIpChange = (event) => {
        this.setState({remote_vpn_ip: event.target.value});
    }
    handleRemoteVpnPortChange = (event) => {
        this.setState({remote_vpn_port: event.target.value});
    }
    handleLoginChange = (event) => {
        this.setState({login: event.target.value});
    }
    handleCaServerChange = (event) => {
        this.setState({ca_server: event.target.value});
    }
    handleCaClientChange = (event) => {
        this.setState({ca_client: event.target.value});
    }
    handleClientKeyChange = (event) => {
        this.setState({client_key: event.target.value});
    }
    handleServerVpnIpChange = (event) => {
        this.setState({server_vpn_ip: event.target.value});
    }
    handleParametersChange = (event) => {
        this.setState({parameters: event.target.value});
    }*/

    render() {
        return (
            <div id="profile">
                <Header/>
            <View style={styles.container}
                   onKeyPress={this.handleKeyPress}>
                <TextInput
                    style={{height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center', backgroundColor: 'white'}}
                    placeholder={'Nom'}
                    value={""}
                    onChange={(event) => this.handleChangeInput("name", event.target.value)}
                />
                <TextInput
                    style={{height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center', backgroundColor: 'white'}}
                    placeholder={'Login'}
                    value={""}
                    onChange={(event) => this.handleChangeInput("login", event.target.value)}
                />
                <TextInput
                    style={{height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center', backgroundColor: 'white'}}
                    placeholder={'Mot de passe'}
                    value={""}
                    onChange={(event) => this.handleChangeInput("password", event.target.value)}
                />
                <TextInput
                    style={{height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center', backgroundColor: 'white'}}
                    placeholder={'IP Local'}
                    value={""}
                    onChange={(event) => this.handleChangeInput("local_Ip", event.target.value)}
                />
                <TextInput
                    style={{height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center', backgroundColor: 'white'}}
                    placeholder={'Remote VPN IP'}
                    value={""}
                    onChange={(event) => this.handleChangeInput("remote_vpn_ip", event.target.value)}
                />
                <TextInput
                    style={{height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center', backgroundColor: 'white'}}
                    placeholder={'Remote VPN port'}
                    value={""}
                    onChange={(event) => this.handleChangeInput("remote_vpn_port", event.target.value)}
                />
                <TextInput
                    style={{height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center', backgroundColor: 'white'}}
                    placeholder={'CA Serveur'}
                    value={""}
                    onChange={(event) => this.handleChangeInput("ca_server", event.target.value)}
                />
                <TextInput
                    style={{height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center', backgroundColor: 'white'}}
                    placeholder={'CA Client'}
                    value={""}
                    onChange={(event) => this.handleChangeInput("ca_client", event.target.value)}
                />
                <TextInput
                    style={{height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center', backgroundColor: 'white'}}
                    placeholder={'Clé client'}
                    value={""}
                    onChange={(event) => this.handleChangeInput("client_key", event.target.value)}
                />
                <TextInput
                    style={{height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center', backgroundColor: 'white'}}
                    placeholder={'IP serveur VPN'}
                    value={""}
                    onChange={(event) => this.handleChangeInput("server_vpn_ip", event.target.value)}
                />
                <TextInput
                    style={{height: 40, borderColor: 'gray', borderWidth: 1, textAlign: 'center', backgroundColor: 'white'}}
                    placeholder={'Paramètres'}
                    value={""}
                    onChange={(event) => this.handleChangeInput("parameters", event.target.value)}
                />
                <Button
                    title="Valider"
                    color="#e6397e"
                    onPress={() => this.props.onSignIn(this.state.login, this.state.password) == false}
                />
            </View>
        </div>

        );
    }
}

export default ProfileInsert;
