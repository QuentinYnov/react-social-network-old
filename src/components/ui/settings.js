import {PropTypes} from 'prop-types'
import React from 'react';
import '../../App.css';
import Header from '../containers/header';
import Toolbar from '../containers/footer';
import Typography from "@material-ui/core/es/Typography/Typography";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = theme => ({
    workinprogress: {
    textAlign: 'center',
        marginTop: 60,
        fontSize: 30
    }
});

function Settings(props) {

    const { classes } = props;

    return (
        <div>
            <Header/>
            <Typography className={classes.workinprogress}>Work In Progress</Typography>
            <Toolbar/>
        </div>
    );

  /*  return (
        <div>
            <Header/>
            <div className="configOption">
                <div className="colorsOption">
                    <div className="primaryColor">
                        Couleur principale : #
                        <input/>
                    </div>
                    <div className="secondaryColor">
                        Couleur secondaire : #
                        <input/>
                    </div>
                </div>
                <div className="windowOption">
                    <div className="windowHeight">
                        Hauteur :
                        <input/>
                    </div>
                    <div className="windowWidth">
                        Largeur :
                        <input/>
                    </div>
                </div>
            </div>
            <Toolbar/>
        </div>
    );*/


}

Settings.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Settings);

