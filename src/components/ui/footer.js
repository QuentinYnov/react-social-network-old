import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import RestoreIcon from '@material-ui/icons/Restore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';

import {Link} from 'react-router-dom';
import Typography from "@material-ui/core/es/Typography/Typography";

const styles = {
    root: {
        width: '100%',
    },
    footer: {
        position: 'fixed',
        left: 0,
        bottom: 0,
        width: '100%',
        backgroundColor: '#f1f1f1',
        color: '#ffffff',
        textAlign: 'center',
        minHeight: 64,
        borderTop: '1px solid #AAAAAA',
        fontSize: 12
    },
    footer_li: {
        color: '#5f5f5f',
        fontSize: 12,
        margin: 3
    },
    footer_li_copyright: {
        color: '#5f5f5f',
        fontSize: 10,
        margin: 3
    }
};

class Footer extends React.Component {
    state = {};

    render() {
        const { classes } = this.props;

        return (
            <div className={classes.footer}>
                <ul className="ul_footer">
                    <li>
                        <Link to="/">
                            <Typography className={classes.footer_li}>A propos de nous</Typography>
                        </Link>
                    </li>
                    <li>
                        <Link to="/">
                            <Typography className={classes.footer_li}>Support</Typography>
                        </Link>
                    </li>
                    <li>
                        <Link to="/">
                            <Typography className={classes.footer_li}>Politique de confidentialité</Typography>
                        </Link>
                    </li>
                    <li>
                        <Link to="/">
                            <Typography className={classes.footer_li}>Publicités</Typography>
                        </Link>
                    </li>
                    <li>
                        <Link to="/">
                            <Typography className={classes.footer_li}>Paramètres</Typography>
                        </Link>
                    </li>
                    <li>
                        <Typography className={classes.footer_li_copyright}>© 2019 REACT Social Network</Typography>
                    </li>
                </ul>
            </div>
        );
    }
}

Footer.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);