import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import {sendMessage, writeMessage, resetInput} from "../../action";
import Message from '../ui/message'

const mapStateToProps = (state) => {
    return ({
        msgValue: state.msgValue,
        msgInput: state.msgInput,
    })
};

const mapDispatchToProps = dispatch =>
    ({
        onSendMessage(msgInput) {
            dispatch(
                sendMessage(msgInput)
            );
            dispatch(
                resetInput()
            )
        },

        onWriteMessage(textWritten) {
            dispatch(
                writeMessage(textWritten)
            )

        },



    });


const Container = connect(mapStateToProps, mapDispatchToProps)(Message);

export default withRouter(Container);
