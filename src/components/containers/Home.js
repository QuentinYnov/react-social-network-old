import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import Home from '../ui/Home';
import {addNumber, resetNumber} from "../../action";


const mapStateToProps = (state) =>
    ({
        number: state.number,
    });

const mapDispatchToProps = dispatch =>
    ({
        onAddNumber(numberAdd){
            dispatch(
                addNumber(numberAdd)
            )
        },

        onResetNumber(){
            dispatch(
                resetNumber()
            )
        }
    });

const Container = connect(mapStateToProps, mapDispatchToProps)(Home);

export default withRouter(Container);