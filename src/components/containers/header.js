import Header from '../ui/header';
import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import {disconnect} from "../../action";

const mapStateToProps = (state) =>
    ({
        //statusLogin: state.statusLogin,

    });

const mapDispatchToProps = dispatch =>
    ({
        /*onDisconnect() {
            dispatch(
                disconnect()
            )
        }*/
    });


const Container = connect(mapStateToProps, mapDispatchToProps)(Header);

export default withRouter(Container);