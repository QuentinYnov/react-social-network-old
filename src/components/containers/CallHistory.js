import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import CallHistory from "../ui/CallHistory";

const mapStateToProps = () =>
    ({

    });

const mapDispatchToProps = dispatch =>
    ({

    });

const Container =  connect(mapStateToProps, mapDispatchToProps)(CallHistory);

export default withRouter(Container);