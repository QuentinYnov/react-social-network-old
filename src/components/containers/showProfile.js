import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import Profile from '../ui/profile';


const Container = connect()(Profile);

export default withRouter(Container);