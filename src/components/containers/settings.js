import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import Settings from '../ui/settings';


const Container = connect()(Settings);

export default withRouter(Container);