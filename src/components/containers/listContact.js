import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import ListContact from '../ui/listContact';
import {fetchContact} from "../../action";

const mapStateToProps = (state) =>{
    console.log(state)
    return ({
        contacts: state.contact.contacts,
        fetching: state.contact.fetching,
        statusLogin: state.statusLogin,
    })};

const mapDispatchToProps = dispatch =>
    ({
        onLoadContact(){
            dispatch(
                fetchContact(),
            )
        }

    });


const Container =  connect(mapStateToProps, mapDispatchToProps)(ListContact);

export default withRouter(Container);