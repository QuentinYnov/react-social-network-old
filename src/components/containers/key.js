import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import {addNumber} from "../../action";
import Key from '../ui/key';

const mapStateToProps = (state, props) =>
    ({
        keyValue: props.keyValue,

    });

const mapDispatchToProps = dispatch =>
    ({
        onAddNumber(numberAdd){
            dispatch(
                addNumber(numberAdd)
            )
        },


    });

const Container = connect(mapStateToProps, mapDispatchToProps)(Key);

export default withRouter(Container);

