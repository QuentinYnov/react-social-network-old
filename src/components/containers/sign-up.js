import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import SignUp from '../ui/sign-up';
import {onLogin} from "../../action";

const mapStateToProps = (state) =>
    ({
        fetchLogin: state.fetchLogin,
        statusLogin: state.statusLogin,
    });

const mapDispatchToProps = dispatch =>
    ({
        onSignIn(login, password){
            dispatch(
                onLogin(login, password)
            )
        }
    });

const Container = connect(mapStateToProps, mapDispatchToProps)(SignUp);

export default withRouter(Container);