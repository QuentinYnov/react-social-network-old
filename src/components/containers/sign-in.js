import {withRouter} from 'react-router';
import {connect} from 'react-redux';
import SignIn from '../ui/sign-in';
import {onLogin, disconnect, pushRoute} from "../../action";

const mapStateToProps = (state) =>
    ({
        fetchLogin: state.fetchLogin,
        statusLogin: state.statusLogin,
        msgValue: state.msgValue,
    });

const mapDispatchToProps = dispatch => ownProps =>
    ({
       /*onSignIn(login, password, msg){
           dispatch(
               onLogin(login, password)
           )
       }*/
        onSignIn(login, password){
            dispatch(
                onLogin(login, password)
            )
        },
        disconnect(){
            dispatch(
                disconnect()
            )
        },
        pushRoute(path, statusLogin){
            dispatch(
                pushRoute(path, statusLogin)
            )
        },

    });

const Container = connect(mapStateToProps, mapDispatchToProps)(SignIn);

export default withRouter(Container);